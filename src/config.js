export const server = {
    port: process.env.PORT || 8080,
}
export const mongo = {
    host: process.env.MONGO_HOST || '127.0.0.1',
    port: process.env.MONGO_PORT || 27017,
    collectionName: process.env.MONGO_DB_NAME || 'api',
    debug: true //will show all action that mongo do
};
export const jwt = {
    secret: process.env.JWT_SECRET || 'some_secretData',
    accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || 86400, //1d
    refreshTokenExpiration: process.env.REFRESH_TOKEN_EXPIRATION || 604800, //1w
};