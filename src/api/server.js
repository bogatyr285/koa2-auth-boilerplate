import Koa from 'koa';
import router from './router'
import KoaBody from 'koa-body'

import { server as servCfg } from '../config'
import { setRequestId, responseTime, errorsHandler, } from './middlewares'

import Logger from '../logger';
const log = Logger('server');

const setup = () => {
    const server = new Koa();
    return server
        .use(setRequestId)
        .use(responseTime)
        .use(errorsHandler)
        .use(KoaBody())
        .use(router.routes())
        .use(router.allowedMethods())
        .listen(servCfg.port, () => { log.info(`Server running. Port: ${servCfg.port}. Environment: ${process.env.NODE_ENV} `); })
}

export default setup