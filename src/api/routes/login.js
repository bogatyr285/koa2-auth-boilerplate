import { User } from '../../db/models'
import Token from '../auth/token'

import Logger from '../../logger'
const log = Logger('Login-Handler');

const loginHandler = async ctx => {
    log.info(`#${ctx.state.reqId}. Handle request from IP: ${ctx.request.ip}.`);
    const { email, password } = ctx.request.body;

    let user = await User.findOne({ email: email });
    if (!user) {
        ctx.throw(400, 'Invalid email')
    }
    log.debug(`#${ctx.state.reqId}. User found: ${user.email}`)
    let isAuthorized = await user.hasValidPassword(password)
    if (!isAuthorized) {
        ctx.throw(400, 'Invalid password')
    }
    let tokens = await Token.generateTokens(user.toJSON())

    log.info(`#${ctx.state.reqId}. Successful login. User: ${user.email}`)
    ctx.body = {
        status: 'ok',
        response: { message: 'Login successful', tokens: tokens }
    };
}

export default loginHandler