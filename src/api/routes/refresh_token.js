import Token from '../auth/token'
import Logger from '../../logger';
const log = Logger('RefreshToken-Handler');

const refreshTokenHandler = async ctx => {
    log.info(`#${ctx.state.reqId}. Handle request from IP: ${ctx.request.ip}.`);

    const { token } = ctx.request.body;

    let newTokens = await Token.generateTokens(ctx.state.user.toJSON());

    log.info(`#${ctx.state.reqId}. Successful token refreshing. User: ${ctx.state.user.email}`)
    ctx.body = {
        status: 'ok',
        response: {
            message: 'Tokens updated',
            tokens: newTokens
        }
    };
}

export default refreshTokenHandler;