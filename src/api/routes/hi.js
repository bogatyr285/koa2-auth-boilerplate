import Logger from '../../logger';
const log = Logger('Hi-Handler');

const hiHander = async ctx => {
    ctx.body = {
        status: 'ok',
        response: {
            user: ctx.state.user
        }
    }
};

export default hiHander