import { User } from '../../db/models'
import Token from '../auth/token'

import Logger from '../../logger';
const log = Logger('Register-Handler');

const registrationHandler = async ctx => {
    log.info(`#${ctx.state.reqId}. Handle request from IP: ${ctx.request.ip}.`);

    const { username, password, email } = ctx.request.body;
    const user = new User({
        username,
        password,
        email
    });
    try {
        await user.save()
    } catch (error) {
        if (error.code === 11000) {
            ctx.throw(409, 'Email already exist')
        }
        ctx.throw(400, error)
    }
    ctx.status = 201;
    ctx.body = {
        status: 'ok',
        response: { message: 'Register successful', user: user.toJSON() }
    };

    log.info(`#${ctx.state.reqId}. Successful register. User: ${user.email}`)
}

export default registrationHandler