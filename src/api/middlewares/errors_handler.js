import Logger from '../../logger';
const log = Logger('Errors-handler');

const errorHandler = async(ctx, next) => {
    try {
        await next();
    } catch (err) {
        //   console.log(`Handling error: `, err); //!!!FOR DEBUG PURPOSES. используется console.log т.к winston не отображают инфу о ошибке красиво
        let msg = {
            name: err.name,
            message: err.message,
            errors: err.errors
        };

        if ((err instanceof Error || err.stack) && process.env.NODE_ENV != 'production') { //!!!FOR DEBUG PURPOSES add stack trace
            msg = Object.assign(msg, { stack: err.stack });
        }

        log.error(`#${ctx.state.reqId}. ${err.type?err.type:'Error occured'}.Reason: ${JSON.stringify(msg, null, 2)}`);

        ctx.status = err.status || 500;

        ctx.response.body = {
            status: 'error',
            error: msg
        };

        return ctx
    }
}

export default errorHandler