import Logger from '../../logger';
const log = Logger('Respose-time');

const responseTime = async(ctx, next) => {
    const start = Date.now();
    await next();
    const delta = Math.ceil(Date.now() - start);
    ctx.set('X-Response-Time', `${delta} ms`);
    log.info(`#${ctx.state.reqId}. Response time: ${delta} ms`)
}

export default responseTime