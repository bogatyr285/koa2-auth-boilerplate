import { User, RefreshToken } from '../../db/models'
import Token from '../auth/token'

import Logger from '../../logger';
const log = Logger('Auth-check');

const authCheck = async(ctx, next) => {
    const { authorization } = ctx.headers;
    log.debug(`#${ctx.state.reqId}. Checking token: ${authorization}`);
    if (!authorization) {
        ctx.throw(400, `Didn't provide access token`);
    }
    const [tokenType, tokenBody] = authorization.split(' '); //0 - header(Refresh or Access); 1 - token body

    if (!tokenType.match(/^Access$/) && !tokenType.match(/^Refresh$/)) {
        ctx.throw(400, `Unknown token type`);
    }
    let user;
    switch (tokenType) {
        case 'Access':
            let payload;
            try {
                payload = await Token.getPayload(tokenBody);
            } catch (error) {
                ctx.throw(401, error.message)
            }

            log.debug(`#${ctx.state.reqId}. Token payload: ${JSON.stringify(payload)}`);
            user = await User.findById(payload.user._id)
            if (!user) {
                ctx.throw(401, 'User not found')
            }
            ctx.state.user = user; // put info about current user to ctx. All next middlewares will be able to access this field 
            break;
        case 'Refresh':
            const uuidRegexp = /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
            if (!tokenBody.match(uuidRegexp)) {
                ctx.throw(400, 'Invalid refresh token format')
            }
            const tokenInfo = await RefreshToken.findOne({ 'token': tokenBody });
            if (!tokenInfo) {
                ctx.throw(401, 'Expired or unexisted token')
            }
            user = await User.findById(tokenInfo.user);
            ctx.state.user = user;
            break;
    }
    log.info(`#${ctx.state.reqId}. Authentication sucessfully. Email: ${user.email}. id:${user._id}`);

    await next();
}

export default authCheck;