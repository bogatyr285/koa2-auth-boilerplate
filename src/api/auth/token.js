import jwt from 'jsonwebtoken'
import bluebird from 'bluebird'
import uuidv4 from 'uuid/v4'

import RefreshToken from '../../db/models/refresh_token'
import { jwt as jwtCfg } from '../../config'
bluebird.promisifyAll(jwt)

const secretKey = jwtCfg.secret;
const accessExpiration = jwtCfg.accessTokenExpiration;
const refreshExpiration = jwtCfg.refreshExpiration;
async function generateTokens(user) {
    const access = jwt.sign({
        user,
        iss: 'https://yourAwesome.site'
    }, secretKey, { expiresIn: accessExpiration })

    await RefreshToken.findOneAndRemove({ user: user._id });
    const refresh = uuidv4();

    await RefreshToken.create({ token: refresh, user: user._id });
    const tokens = {
        access,
        refresh,
        expires_in: jwt.decode(access).exp,
    }
    return tokens
}
async function getPayload(token) {
    try {
        const payload = await jwt.verifyAsync(token, secretKey)
        return payload
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            throw { name: 'UnauthorizedError', message: `Token was expired at: ${error.expiredAt}` }
        }
        if (error.name === 'JsonWebTokenError') {
            throw { name: 'UnauthorizedError', message: `Invalid token` }
        }
        console.log('!!!Cannot verify token :', token, error) // судя по документации до сюда не должно доходить. Но на всякий пожарный пусть будет
        throw { name: 'UnauthorizedError', message: `Invalid token.` }
    }
}
export default { generateTokens, getPayload }