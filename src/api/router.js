 import Router from 'koa-router';

 import { isAuthorized } from './middlewares';
 import { hiHandler, loginHandler, registrationHandler, refreshTokenHandler } from './routes';
 const router = new Router({ prefix: '/api' });
 //curl -X GET -H 'Authorization: Access #TOKEN' http://localhost:8080/hi
 router.get('/hi', isAuthorized, hiHandler);
 //curl -X POST -d 'username=test&password=test&email=test@test.com' http://localhost:8080/register
 router.post('/auth/register', registrationHandler);
 //curl -X POST -d 'password=test&email=test@test.com' http://localhost:8080/login
 router.post('/auth/login', loginHandler);
 //curl -X POST -H 'Authorization: Refresh 93ea8dd1-c055-44a7-9839-80f2bc907fc3' http://localhost:8080/refresh 
 router.post('/auth/refresh', isAuthorized, refreshTokenHandler);
 // router.get('/', mainPage);

 export default router;