import bluebird from 'bluebird';

import dbSetup from './db';
import apiSetup from './api';
import Logger from './logger';
const log = Logger('app');

global.Promise = bluebird.Promise;

process.on('uncaughtException', error => {
    log.error('Unhandled exeption:')
    log.error(`${error.name} ${error.message}`);
});
process.on('unhandledRejection', error => {
    log.error('Unhandled rejection:')
    log.error(`${error.name} ${error.message}`);
});

(async() => {
    try {
        await dbSetup();
        await apiSetup();
    } catch (error) {
        console.log('Something went wrong:\n', error)
        process.exit(1)
    }
})();