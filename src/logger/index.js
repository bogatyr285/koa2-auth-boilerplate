import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, printf, prettyPrint } = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${JSON.stringify(info.message,null,2)}`;
});

export default (who) => {
    return createLogger({
        format: combine(label({ label: who }), timestamp(), format.colorize(), prettyPrint(), myFormat),
        transports: [
            new transports.Console({
                level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
            }),
        ],
    })
}