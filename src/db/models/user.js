import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import { mongo as mongoCfg } from '../../config';

import Logger from '../../logger';
const log = Logger('UserSchema');

const SALT_ROUNDS = 10;

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: `Username didn't specified`,
    },
    email: {
        type: String,
        required: `Email didn't specified`,
        unique: true,
        lowercase: true,
        match: [/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
            `Please provide a valid email address`
        ]
    },
    password: {
        type: String,
        required: true
    },
    salt: String
}, {
    timestamps: true,
    toJSON: {
        transform: (doc, ret, options) => {
            delete ret.password;
            delete ret.salt;
            delete ret.__v;
        }
    }
});

UserSchema.pre('save', async function preSave(next) {
    const user = this;

    // only if password was modified
    if (!user.isModified('password')) {
        next();
    }

    try {
        user.salt = await bcrypt.genSaltSync(SALT_ROUNDS); //default 10 rounds
        user.password = await bcrypt.hashSync(user.password, user.salt);
        next();
    } catch (error) {
        log.error(`${error.name} ${error.message}`);
        next(error);
    }
});

UserSchema.methods.hasValidPassword = function(testPassword) {
    if (!testPassword || !this.password) return false;
    return bcrypt.compareSync(testPassword, this.password)
};

export default mongoose.model('User', UserSchema);