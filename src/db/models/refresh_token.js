import mongoose from 'mongoose';
import { jwt as cfg } from './../../config';

import Logger from '../../logger';
const log = Logger('RefreshTokenSchema');

const RefreshTokenSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        unique: true,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created_at: {
        type: Date,
        expires: cfg.refreshTokenExpiration
    }
}, { timestamps: true });

export default mongoose.model('RefreshToken', RefreshTokenSchema);