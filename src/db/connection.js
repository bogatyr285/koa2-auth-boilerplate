import mongoose from 'mongoose'
import bluebird from 'bluebird'

import { mongo as mongoCfg } from '../config'
import Logger from '../logger';
const log = Logger('DB');

const mongoUrl = `mongodb://${mongoCfg.host}:${mongoCfg.port}/${mongoCfg.collectionName}`

bluebird.promisifyAll(mongoose)

const setup = () => {
    mongoCfg.debug ? mongoose.set('debug', true) : '';
    return mongoose.connect(mongoUrl, (err) => {
        if (err) {
            log.error(`Mongo connection error: ${err.message}`, )
            throw { name: 'MONGO_CONN_ERR', message: err.message }
        }
        mongoose.Promise = global.Promise;

        log.info(`Mongo connected to ${mongoUrl}`);
    });
}

export default setup