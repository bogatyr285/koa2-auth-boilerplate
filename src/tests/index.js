import chai from 'chai';
import chaiHttp from 'chai-http';
import importDir from 'import-dir';

import dbSetup from '../db';
import api from '../api';

import { mongo as mongoCfg } from '../config';
const should = chai.should();
const assert = chai.assert;
chai.use(chaiHttp);
let request;

let context = {
    user: {
        valid: {
            email: 'test@test.com',
            username: 'user',
            password: 'pswd'
        },
        tokens: { //there'll be tokens after sucessful login 
            valid: {
                access: '',
                refresh: ''
            },
            previous: { //store used tokens for test updating tokens in DB
                access: '',
                refresh: ''
            }
        }
    },

};
describe('API test', () => {
    before(async() => {
        try {
            await dbSetup();
            request = chai.request.agent(await api());
        } catch (error) {
            console.log('Unable init app', error);
        }
    });
    describe('Registration', () => {
        it('should throw 400 if email is incorrect', async() => {
            request
                .post('/api/auth/register')
                .send({ email: 'wrong@email', username: context.user.valid.username, password: context.user.valid.password })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'ValidationError');
                })
        });
        it('should register a new user', async() => {
            request
                .post('/api/auth/register')
                .send({ email: context.user.valid.email, username: context.user.valid.username, password: context.user.valid.password })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.have.deep.property('status', 'ok');
                    res.body.response.should.to.have.all.keys('message', 'user')
                })
        });
        it('should throw 409 if user already exist', async() => {
            request
                .post('/api/auth/register')
                .send({ email: 'test@test.com', username: 'user', password: 'pswd' })
                .end((err, res) => {
                    res.should.have.status(409);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'ConflictError');
                })
        });
    });
    describe('Login', () => {
        it('should throw 400 if email is incorrect', async() => {
            request
                .post('/api/auth/login')
                .send({ email: 'wrong@email', username: 'user', password: 'pswd' })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'BadRequestError');
                })
        });
        it('should login a user', async() => {
            request
                .post('/api/auth/login')
                .send({ email: context.user.valid.email, password: context.user.valid.password })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.deep.property('status', 'ok');
                    res.body.response.should.to.have.all.keys('message', 'tokens');
                    res.body.response.tokens.should.to.have.all.keys('access', 'refresh', 'expires_in');
                    context.user.tokens.valid = res.body.response.tokens;
                })
        });
    });
    describe('Refresh tokens', () => {
        it('should throw 400 if didn\'t provide headers', async() => {
            request
                .post('/api/auth/refresh')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'BadRequestError');
                })
        });
        it('should throw 401 if invalid refresh token', async() => {
            request
                .post('/api/auth/refresh')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', 'Refresh 00000000-0000-4000-bd43-e1c9be3e29cf')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'UnauthorizedError');
                })
        });
        it('should refresh tokens', async() => {
            request
                .post('/api/auth/refresh')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Refresh ${context.user.tokens.valid.refresh}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.deep.property('status', 'ok');
                    res.body.response.should.to.have.all.keys('message', 'tokens');
                    res.body.response.tokens.should.to.have.all.keys('access', 'refresh', 'expires_in');

                    context.user.tokens.previous = context.user.tokens.valid;
                    context.user.tokens.valid = res.body.response.tokens;
                })
        });
        it('should throw 401 if token was used', async() => {
            await new Promise(resolve => setTimeout(() => resolve(), 500)); //BICYCLE POWER. wait for data from previous test
            request
                .post('/api/auth/refresh')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Refresh ${context.user.tokens.previous.refresh}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'UnauthorizedError');
                })
        });
    });
    describe('Fetch data', () => {
        it('should throw 400 if didn\'t provide headers', async() => {
            request
                .get('/api/hi')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'BadRequestError');
                })
        });
        it('should throw 401 if invalid access token', async() => {
            request
                .get('/api/hi')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', 'Access  wrongAccessToken')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.have.deep.property('status', 'error');
                    assert.equal(res.body.error.name, 'UnauthorizedError');
                })
        });
        it('should fetch data', async() => {
            request
                .get('/api/hi')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Access ${context.user.tokens.valid.access}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.deep.property('status', 'ok');
                    res.body.response.should.to.have.all.keys('user');
                })
        });
    });



    // after(() => {
    //     const routes = importDir('./routes');
    //     Object.keys(routes).forEach(name => routes[name](request, context));
    // });
});