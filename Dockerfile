FROM mhart/alpine-node

RUN mkdir -p /projects/koa-auth
WORKDIR /projects/koa-auth

COPY package.json /projects/koa-auth/package.json
RUN cd /projects/koa-auth \
	&& echo ">> Installing NPM packages" \
    && npm i     
COPY . /projects/koa-auth/
RUN npm run build